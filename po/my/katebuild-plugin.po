# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-08-24 00:42+0000\n"
"PO-Revision-Date: 2021-09-15 04:13+0630\n"
"Last-Translator: \n"
"Language-Team: Burmese <kde-i18n-doc@kde.org>\n"
"Language: my\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ဇေယျာလွင်"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lw1nzayar@yandex.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "အထုတ်"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "ထပ်၍ တည်ဆောက်မည်"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "ပယ်ဖျက်မည်"

#: buildconfig.cpp:47
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build & Run"
msgstr "ထပ်၍ တည်ဆောက်မည်"

#: buildconfig.cpp:53
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build & Run Settings"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: buildconfig.cpp:103
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Delete selected entries"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: buildconfig.cpp:108
#, kde-format
msgid "Delete all entries"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: buildconfigwidget.ui:33
#, fuzzy, kde-format
#| msgctxt "Tab label"
#| msgid "Target Settings"
msgid "General Settings"
msgstr "ဦိးတည်ပစ်မှတ် ဆက်တင်"

#. i18n: ectx: property (text), widget (QCheckBox, useDiagnosticsCB)
#: buildconfigwidget.ui:50
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, autoSwitchToOutput)
#: buildconfigwidget.ui:57
#, kde-format
msgid "Automatically switch to output pane on executing the selected target"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: buildconfigwidget.ui:96
#, kde-format
msgid "Allowed && Blocked Commands"
msgstr ""

#: plugin_katebuild.cpp:272 plugin_katebuild.cpp:279 plugin_katebuild.cpp:709
#: plugin_katebuild.cpp:911 plugin_katebuild.cpp:924
#, kde-format
msgid "Build"
msgstr "တည်ဆောက်မည်"

#: plugin_katebuild.cpp:282
#, kde-format
msgid "Select Target..."
msgstr "ဦိးတည်ပစ်မှတ် ရွေးမည်..."

#: plugin_katebuild.cpp:287
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build Selected Target"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: plugin_katebuild.cpp:292
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and Run Selected Target"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: plugin_katebuild.cpp:297
#, kde-format
msgid "Compile Current File"
msgstr ""

#: plugin_katebuild.cpp:298
#, kde-format
msgid "Try to compile the current file by searching a compile_commands.json"
msgstr ""

#: plugin_katebuild.cpp:303
#, kde-format
msgid "Stop"
msgstr "ရပ်မည်"

#: plugin_katebuild.cpp:308
#, kde-format
msgid "Load targets from CMake Build Dir"
msgstr ""

#: plugin_katebuild.cpp:312
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:332
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:354
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "ဦိးတည်ပစ်မှတ် ဆက်တင်"

#: plugin_katebuild.cpp:394
#, kde-format
msgid ""
"<b>File not found:</b> %1<br><b>Search paths:</b><br>%2<br>Try adding a "
"search path to the \"Working Directory\""
msgstr ""

#: plugin_katebuild.cpp:477
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build Information"
msgstr "ထပ်၍ တည်ဆောက်မည်"

#: plugin_katebuild.cpp:716
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "တည်ဆောက်ရန် ဖိုင်လ် သို့မဟုတ် ဒါရိုက်ထရီ သတ်မှတ်ထားခြင်းမရှိပါ။"

#: plugin_katebuild.cpp:720
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""

#: plugin_katebuild.cpp:782
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:796
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "\"%1\" ကို မမောင်းနှင်နိုင်ခဲ့ပါ။ အထွက်အခြေအနေ = %2"

#: plugin_katebuild.cpp:811
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "တည်ဆောက်မှု <b>%1</b> ပယ်ဖျက်ခဲ့သည်"

#: plugin_katebuild.cpp:910
#, kde-format
msgid "Did not find a compile_commands.json for file \"%1\". "
msgstr ""

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Did not find a compile command for file \"%1\" in \"%2\". "
msgstr ""

#: plugin_katebuild.cpp:1031
#, kde-format
msgid "No target available for building."
msgstr "တည်ဆောက်ရန် ဦးတည်ပစ်မှတ် မရှိပါ"

#: plugin_katebuild.cpp:1045
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr ""

#: plugin_katebuild.cpp:1051
#, kde-format
msgid "Already building..."
msgstr "တည်ဆောက်နေပြီးဖြစ်သည်..."

#: plugin_katebuild.cpp:1073
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "<b>%1</b> ဦးတည်ပစ်မှတ် တည်ဆောက်နေသည်..."

#: plugin_katebuild.cpp:1110
#, kde-format
msgid ""
"Cannot load targets, the file %1 does not contain a proper CMAKE_COMMAND "
"entry !"
msgstr ""

#: plugin_katebuild.cpp:1123
#, kde-format
msgid "Could not write CMake File API query files for build directory %1 !"
msgstr ""

#: plugin_katebuild.cpp:1129
#, kde-format
msgid "Could not run CMake (%2) for build directory %1 !"
msgstr ""

#: plugin_katebuild.cpp:1138
#, kde-format
msgid ""
"Generating CMake File API reply files for build directory %1 failed (using "
"%2) !"
msgstr ""

#: plugin_katebuild.cpp:1257
#, kde-format
msgid "Build plugin wants to execute program"
msgstr ""

#: plugin_katebuild.cpp:1260
#, kde-format
msgid ""
"The Kate build plugin needs to execute an external command to read the "
"targets from the build tree.<br><br>The full command line is:<br><br><b>%1</"
"b><br><br>Proceed and allow to run this command ?<br><br>The choice can be "
"altered via the config page of the plugin."
msgstr ""

#: plugin_katebuild.cpp:1292
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:1328
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:1334
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "အမှား %1ခု တွေ့ရှိသည်။"

#: plugin_katebuild.cpp:1338
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "သတိပေးချက် %1ခု တွေ့ရှိသည်"

#: plugin_katebuild.cpp:1341
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "အမှား %1ခု တွေ့ရှိသည်။"

#: plugin_katebuild.cpp:1346
#, kde-format
msgid "Build failed."
msgstr "တည်ဆောက်မှု မအောင်မြင်ပါ။"

#: plugin_katebuild.cpp:1348
#, kde-format
msgid "Build completed without problems."
msgstr ""

#: plugin_katebuild.cpp:1353
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:1377
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1603
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "အမှား"

#: plugin_katebuild.cpp:1606
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "သတိ"

#: plugin_katebuild.cpp:1609
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1612
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "မသတ်မှတ်ထားသော အကိုးအကား"

#: plugin_katebuild.cpp:1788
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:51
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>ပစ်မှတ် -</B> %1"

#: TargetHtmlDelegate.cpp:53
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>ဒါရိုက် -</B> %1"

#: TargetHtmlDelegate.cpp:104
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""

#: TargetHtmlDelegate.cpp:111
#, kde-format
msgid ""
"Use:\n"
"\"%B\" for project base directory\n"
"\"%b\" for name of project base directory"
msgstr ""

#: TargetHtmlDelegate.cpp:114
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""

#: TargetModel.cpp:310 TargetModel.cpp:322 targets.cpp:247
#, kde-format
msgid "Target Set"
msgstr "ဦိးတည်ပစ်မှတ်အစု"

#: TargetModel.cpp:513
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:513
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:607
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:610
#, kde-format
msgid "Working Directory / Command"
msgstr "အလုပ်လုပ်မည့်ဒါရိုက်ထရီ / အမိန့်"

#: TargetModel.cpp:613
#, kde-format
msgid "Run Command"
msgstr ""

#: targets.cpp:29
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:33
#, fuzzy, kde-format
msgid "Create new set of targets"
msgstr "ကုတ်ကိုင်း အသစ်တည်ဆောက်မည်"

#: targets.cpp:37
#, kde-format
msgid "Clone command or target set"
msgstr ""

#: targets.cpp:41
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:46
#, kde-format
msgid "Add new target"
msgstr "ဦးတည်ပစ်မှတ်အသစ် ထည့်မည်"

#: targets.cpp:50
#, kde-format
msgid "Build selected target"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: targets.cpp:54
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and run selected target"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: targets.cpp:58
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target up"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: targets.cpp:62
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target down"
msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#: targets.cpp:201
#, kde-format
msgid "Copy"
msgstr ""

#: targets.cpp:205
#, kde-format
msgid "Cut"
msgstr ""

#: targets.cpp:209
#, kde-format
msgid "Paste after"
msgstr ""

#: targets.cpp:227 targets.cpp:249
#, fuzzy, kde-format
#| msgid "Build again"
msgctxt "Name/Label for a compilation or build command"
msgid "Build Command"
msgstr "ထပ်၍ တည်ဆောက်မည်"

#: targets.cpp:248
#, fuzzy, kde-format
#| msgid "Config"
msgctxt "Name/Label for a command to configure a build"
msgid "Configure"
msgstr "ကွန်ဖစ်"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Bတည်ဆောက်မည်"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "လမ်းကြောင်းထည့်သွင်းမည်"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "ထည့်သွင်းရန် ဒါရိုက်ထရီ ရွေးမည်"

#~ msgid "Clean"
#~ msgstr "ရှင်းမည်"

#, fuzzy
#~ msgid "Project Plugin Targets"
#~ msgstr "ဦးတည်းဖိုင်လ်များကို အညွှန်းစီမည်"

#~ msgid "build"
#~ msgstr "တည်ဆောက်မည်"

#~ msgid "clean"
#~ msgstr "ရှင်းမည်"

#~ msgid "quick"
#~ msgstr "အလျင်အမြန်"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "<b>%1</b> တည်ဆောက်မှု ပြီးပါပြီ"

#~ msgid "Show:"
#~ msgstr "ပြမည် -"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "ဖိုင်လ်"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "စာကြောင်း"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "မှတ်စာ"

#~ msgid "Next Error"
#~ msgstr "နောက်အမှား"

#~ msgid "Previous Error"
#~ msgstr "ရှေ့အမှား"

#~ msgid "Show Marks"
#~ msgstr "အမှတ်အသားများ ပြမည်"

#~ msgid "Error"
#~ msgstr "အမှား"

#~ msgid "Warning"
#~ msgstr "သတိ"

#~ msgid "Only Errors"
#~ msgstr "အမှားများသာ"

#~ msgid "Errors and Warnings"
#~ msgstr "အမှားနှင့် သတိပေးချက်များ"

#, fuzzy
#~ msgid "Parsed Output"
#~ msgstr "အထုတ်"

#, fuzzy
#~ msgid "Full Output"
#~ msgstr "အထုတ်"

#~ msgid "Select active target set"
#~ msgstr "သက်ဝင်သော ဦိးတည်ပစ်မှတ်အစု ရွေးပါ။"

#, fuzzy
#~| msgid "Build selected target"
#~ msgid "Filter targets"
#~ msgstr "ရွေးထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#~ msgid "Build Default Target"
#~ msgstr "မူလသတ်မှတ်ထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "မူလသတ်မှတ်ထားသော ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#~ msgid "Build Previous Target"
#~ msgstr "ရှေ့ဦးတည်ပစ်မှတ် တည်ဆောက်မည်"

#~ msgid "Active target-set:"
#~ msgstr "သက်ဝင်သော ဦိးတည်ပစ်မှတ်အစု -"

#~ msgid "config"
#~ msgstr "ကွန်ဖစ်"

#~ msgid "Kate Build Plugin"
#~ msgstr "ကိတ် တည်ဆောက်မှု ပလပ်ဂင်"

#~ msgid "Select build target"
#~ msgstr "တည်ဆောက်မှု ဦိးတည်ပစ်မှတ် ရွေးမည်"

#~ msgid "Filter"
#~ msgstr "စစ်ထုတ်မည်"

#, fuzzy
#~ msgid "Build Output"
#~ msgstr "ပတ်ကေ့ချ်ကို မဆောက်နိုင်ခဲ့ပါ"
